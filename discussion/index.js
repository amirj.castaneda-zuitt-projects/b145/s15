console.log("Hello from JS");

// Error message
console.error('This is an error');
// warning message
console.warn("I'm warning you!");

function errorMessage() {
	console.error('This is an Error!')
};

errorMessage();

function greetings() {
	console.log('Salutation from JS!')
}

greetings();

let givenName = "John";
let familyName = "Smith";

function fullName() {
	console.log(givenName + ' ' +familyName);
}
fullName();

function computeTotal() {
	let numA = 20;
	let numB = 5;
	console.log(numA + numB);
}
computeTotal();

// Functions with parameters.
function addName(first, last) {
	console.log(first + ' ' + last + ' END');
}

addName(givenName, familyName);

function dialog() {
	console.log("Ooops! I dit it again");
	console.log("Don't you know that you're toxic");
	console.log("Sometimes I run");
	return "I am a slave"
}

// You called dialog(); It will print the console.log statements. But it will not print the I am slave since it has been assigned to var x.
var x = dialog();
console.log(x);

console.log(dialog());

// We will create a simple function to be used as an argument later.
function argumentSaFunction() {
	console.log("This function was passed as an argument")
}

function invokeFunction(argumentNaFunction) {
	argumentNaFunction();
	console.log('Let us print this after we call the argumentSafunction')
}

invokeFunction(argumentSaFunction)